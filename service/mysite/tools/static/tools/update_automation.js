$("#staging_table").DataTable({
    columns: [
             {"data": "SectionKeyID"},
             {"data": "Automation Status"},
             {"data": "Reason"},
             {"data": "Status"},

        ]
})

var environment = $("#environment").select2({
    width: 'auto',
    placeholder: "Environment",
    data: [
        {"id": "test", "text": "Test"},
        {"id": "prod", "text": "Prod"}
    ]
})

var section_keyids = $("#sectionkeyid").select2({
    width: 'auto',
    placeholder: "SectionKeyID",
    ajax: {
        url: function () {
            var env = environment.select2("data")[0].id
            return "http://172.16.16.61/api/aims/db_utils/" + env + "/get_section_keys"
        },
        data: function (params) {
          return {
              sectionkeyid: params.term
          }
        },
        processResults: function (data) {
            var parsed_data = []
                var items = data.data
                for (var index = 0; index < items.length; index++) {
                    var current_item = items[index]
                    if ("ItemID" in current_item) {
                        parsed_data.push({
                            "id": current_item,
                            "text": current_item,
                            "name": current_item

                        })
                    }

                }
                return {results: parsed_data};

        }
    }
})

$("#add_sectionkeyid").on("click", function (event) {
    event.preventDefault()


})