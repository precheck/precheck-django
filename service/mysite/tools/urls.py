from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^agent_mover/', views.agent_mover, name='agent_mover'),
    url(r'^update_automation/', views.update_automation, name='update_automation'),
    url(r'update_mappings/', views.update_mappings, name='update_mappings'),
    url(r'^base_template/', views.base_template, name='base_template'),
    url(r'^$', views.tools_index, name='tools_index')
]
