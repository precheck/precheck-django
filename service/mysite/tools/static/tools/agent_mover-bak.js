/**
 * Created by Hramirez on 6/9/2017.
 */
$(function () {
    var moz_server = "http://172.16.16.60:7777"

    var agent_table = $("#move_agent_table").DataTable({
        columns: [
            {"data": "SectionKeyID"},
            {"data": "AgentID"},
            {"data": "Source"},
            {"data": "Target"},
            {"data": "Status"}
        ],
        select: {
            style: 'multi'
        }
    });
    function add_to_table(current_user) {


        var agent_id = $("#agentid_input").val();
        var source_env = $("#source_env").val()
        var target_env = $("#target_env").val()

        if(source_env === target_env) {
            alert("source environment cannot be the same as the target environment")
            return
        }

        if (agent_id != "") {
            $.ajax({
                "method": "GET",
                "url": moz_server+ "/v1/mozenda/agents/is_moving",
                "data": {"agent_id": agent_id, "source_env": source_env},
                success: function (res_1) {
                    if (res_1.data === "No") {
                        $.ajax({
                            "method": "GET",
                            "url": moz_server + "/v1/mozenda/agents/get_section",
                            "data": {"agent_id": agent_id, "source_env": source_env},
                            success: function (res_2) {
                                if (res_2.status === "agent_found") {

                                    var agent_info = {
                                        "AgentID": agent_id,
                                        "Source": source_env,
                                        "Target": target_env,
                                        "SectionKeyID": res_2["LicenseState"] + "-" + res_2["LicenseType"] ,
                                        "Status": "moving"
                                    }
                                    agent_table.row.add(agent_info).draw()
                                     $.ajax({
                                        "method": "POST",
                                         "data": JSON.stringify({"agent_id": agent_id, "source_env": source_env}),
                                         "url": moz_server + "/v1/mozenda/agents/add_all_moved_agents"
                                    })

                                    $.ajax({
                                        "method": "POST",
                                         "data": JSON.stringify({"agent_id": agent_id, "source_env": source_env, "current_user": current_user }),
                                         "url": moz_server + "/v1/mozenda/agents/add_user_moved_agents"
                                    })

                                } else {
                                    alert("Agent ID is not mapped for the current environment")
                                }


                            }

                        })
                    }
                }
            })
        }
    }


    $("#delete_rows").on("click", function () {
        agent_table.rows(".selected").remove().draw()
    })

    $("#clear_table").on("click", function () {
        agent_table.clear().draw()

    })
})