from celery import shared_task
from .task_utils.processor import HandlerFactory, ProcessorFactory
from .task_utils.error_reporter import DataErrorReporter

@shared_task
def add_entries(data, endpoint, env):
    data_rows = data.pop("rows")
    data_handlers = HandlerFactory.get_handlers(data["update_criteria"], data["section"], env)
    recipient = data.pop("recipient")
    current_user = data.pop('current_user')
    response_proccessor = ProcessorFactory.get_processor(env, endpoint)
    response_proccessor.set_error_reporter(DataErrorReporter(from_addr="halcyonramirez@precheck.com",
                                                             recipient=recipient,
                                                             subject="Agent Not Found"))
    for row in response_proccessor.processed_rows(data_rows, data["update_criteria"]):
        for data_handler in data_handlers:
            formatted_data = data_handler.format_data(row)
            data_handler.add_data(formatted_data)

    for data_handler in data_handlers:
        handler_data = data_handler.get_formated_data()
        if handler_data:
            model_updater = ModelUpdate(data_handler.tied_model)
            model_updater.set_error_reporter(DataErrorReporter(from_addr="halcyonramirez@precheck.com",
                                                               recipient=recipient,
                                                               subject="data errors while saving"))
            model_updater.incremental_save(handler_data)
    send_notifs(current_user)
