from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings
# from mysite.main.mysql_models import CcSearchTypes

# Create your views here.

# def get_mappings(request):
#     data = CcSearchTypes.objects.all()
#     print(data)
#     return render("tools/")

# def update_mappings(request):
#     data = request.get_json()["data"]
#     data["recipient"] = "halcyonramirez@precheck.com"
#     data['current_user'] = request.user.email
#     env = data.pop("env").lower()
#     agent_endpoint = current_app.config.get("TEST_ENDPOINT") if env == "test" else current_app.config.get(
#         "PROD_ENDPOINT")
#     # logging.info(agent_endpoint)
#     add_entries.delay(data, agent_endpoint, env)
#     return "success"




# Create your views here.

@login_required(login_url='login')
def tools_index(request):
    return render(request, "tools/tools_index.html")

def agent_mover(request):
    return render(request, "tools/agent_mover.html", {})

def update_automation(request):
    return render(request, "tools/update_automation.html", {})

def update_mappings(request):
    return render(request, "tools/update_mappings.html", {})

def base_template(request):
    return render(request, "tools/base_template.html", {})

