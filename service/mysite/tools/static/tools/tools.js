$(document).ready(function() {
    var t = $('#move_agent_table').DataTable();
    var counter = 1;

    $('#add_button').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );

        counter++;
    } );

    // Automatically add a first row of data
    $('#add_button').click();
} );