var SERVER_URL = "172.16.16.61"

var base_license_table = $("#base_license_table").DataTable({
    columns: [
        {"data": "Id"},
        {"data": "User"},
        {"data": "Environment"},
        {"data": "Section"},
        {"data": "Name"},
        {"data": "LicenseState"},
        {"data": "LicenseType"},
        {"data": "SearchType"},
        {"data": "SearchOrderID"},
        {"data": "agent_name"},
        {"data": "collection_name"},
        {"data": "Status"}
            ],
       columnDefs: [
        {
            targets:[0,9,10],
            visible: false,
            searchable: false
        }

    ],
})


var staging_table = $("#staging_table").DataTable({
    columns: [
        {"data": "Id"},
        {"data": "User"},
        {"data": "Environment"},
        {"data": "Section"},
        {"data": "Name"},
        {"data": "LicenseState"},
        {"data": "LicenseType"},
        {"data": "SearchType"},
        {"data": "SearchOrderID"},
        {"data": "agent_name"},
        {"data": "collection_name"},
        {"data": "Status"}
    ],
    columnDefs: [
        {
            targets:[0,1,9,10,11],
            visible: false,
            searchable: false
        }

    ],

     dom: "Bfrtip",
        buttons: [{
               "text": "Delete",
               "className": "btn btn-warning col-md-offset-2",
               action: function ( e, dt, node, config ) {
                   this.rows(".selected").remove().draw()
               }
           }],
    select: {
            style: 'multi'
        }
})

var env =  $("#env_category").select2({
    data: [
        {id: "test", text:"Test"},
        {id: "prod", text:"Prod"}
    ],
    width: '20%'
})

var section = $("#section_category").select2({
    data: [
        {id: "License", text: "CC"},
        {id: "CRIM", text: "CRIM"}
    ],
    width: '20%'
})

var format_lic_types = function (data) {
         if (data.loading) return data.text;
        var markup = '<div class="row">' +
            '<div class="col-sm-12">' +
                '<div>' + data.id + '  ' + ' -- ' + data.description   +'</div>' +
            '</div>' +
                '</div>'

    return markup;
}


var format_state_codes = function (data) {
         if (data.loading) return data.text;
        var markup = '<div class="row">' +
            '<div class="col-sm-12">' +
                '<div>' + data.id + '  ' + ' -- ' + data.state_name   +'</div>' +
            '</div>' +
                '</div>'

    return markup;
}


var state_code = $("#state_code").select2( {
    dropdownAutoWidth : true,
    width: 'auto',
    minimumInputLength: 1,
    placeholder: "State",
    ajax: {
        url: "http://172.16.16.61/api/aims/db_utils/" + "get_states",
        delay:250,
        data: function (params) {
            return {
                state: params.term
            }
        },
        processResults: function (data, params) {
            var parsed_data = []
            console.log(data)
            for (var index = 0; index < data.data.length; index++) {
                parsed_data.push({
                    "id": data.data[index].abbreviation,
                    "text": data.data[index].abbreviation,
                    "state_name": data.data[index].state

                })
            }
            return {results: parsed_data};
        }
    },
    escapeMarkup: function (markup) { return markup; },
    templateResult: format_state_codes

})

var lic_type  = $("#lic_type").select2({
    dropdownAutoWidth : true,
    width: 'auto',
    minimumInputLength: 1,
    placeholder: "License Type",
    ajax: {
        url: function () {
            var environment = env.select2("data")[0].id
            return "http://172.16.16.61/api/aims/db_utils/" + environment + "/get_licenses"
        },
        contentType: 'json',
        delay: 250,
        data: function (params) {
            return {
                license_type: params.term,
            }

        },
        processResults: function (data, params) {
            var parsed_data = []
            for (var index = 0; index < data.data.length; index++) {
                parsed_data.push({
                    "id": data.data[index].license_type,
                    "text": data.data[index].license_type,
                    "description": data.data[index].description

                })
            }
            return {results: parsed_data};
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    templateResult: format_lic_types
})

var search_type = $("#search_type").select2({
        dropdownAutoWidth: true,
        width: 'auto',
        placeholder: "Search Type",
        ajax: {
            url: function () {
                return "http://172.16.16.61/api/aims/db_utils/search_types"
            },
            contentType: "json",
            delay: 250,
            data: function (params) {
                return {search_type: params.term}

            },
            processResults: function (data, params) {
                var parsed_data = []
                console.log(data.data)
                for (var index = 0; index < data.data.length; index++) {
                    parsed_data.push({
                        "id": data.data[index].search_type,
                        "text": data.data[index].search_type,

                    })
                }
                return {results: parsed_data};
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
})


function add_agent(user) {
    var environment = env.select2("data")[0].text
    var license_type = lic_type.select2("data")[0].text
    var section_category = section.select2("data")[0].id
    var prefix = section_category.toLowerCase() == "license"? "L": "C"
    var search = search_type.select2("data")[0].text
    var state = state_code.select2("data")[0].id
    var agent_name = prefix + "-" + state + "-" + license_type + " --" + search

    var new_data = {
        "Id": agent_name + ":" + $("#search_order").val() + ":"  + environment.toLowerCase(),
        "User": user,
        "Environment": environment,
        "Section": section_category,
        "Name": agent_name,
        "LicenseState": state,
        "SearchType": search,
        "LicenseType": license_type ,
        "SearchOrderID": $("#search_order").val(),
        "agent_name": agent_name,
        "collection_name": agent_name + " Data",
        "Status": "Preparing",
    }
    staging_table.row.add(new_data).draw()


}


var moz_server = "http://172.16.16.61"
var templator_ws = io.connect(moz_server + "/agent_templating", {path: '/moz_sockets'});


function setup_templating (user) {
    var items = staging_table.rows().data().toArray()
    staging_table.clear().draw()
    templator_ws.emit("start_templating", {data: items})

    setTimeout(function() {
         $.ajax({
             "type": "POST",
             "url": moz_server + "/api/tasks/mozenda/agents/make_agent_templates",
             "data": JSON.stringify({"data": items, "user_id": user})})
    }, (3 * 1000));



}


templator_ws.on("move_to_final_table", function (data) {
    base_license_table.rows.add(data["data"]).draw()

})


