from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
url(r'^template/', views.mappings, name='mappings'),
url(r'^login/', auth_views.login, {'template_name': 'home/login.html'}, name='login'),
url(r'^logout/$', lambda requests: auth_views.logout_then_login(requests, 'login'), name='logout'),
# url(r'^logout/'.views.logout, name='logout'),
url(r'^$', views.index, name='index')
]