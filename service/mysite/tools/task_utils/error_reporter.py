import abc
from django.core.mail.message import EmailMessage
class BaseErrorReporter:
    def __init__(self, subject=None,from_addr=None, recipient=None):
        self._recipient = recipient
        self._from_addr = from_addr
        self._subject = subject
        self._email = EmailMessage()
        self._errored_items = list()

    def add_errored_item(self, item):
        self._errored_items.append(item)

    @abc.abstractmethod
    def _compose_mail(self):
        pass

    def send_mail_if_any(self):
        errored_items = self._errored_items
        if errored_items:
            self._compose_mail(errored_items)
            self._email.send()



class DataErrorReporter(BaseErrorReporter):
    def _compose_mail(self, errored_items):
        self._email.body = (str(errored_items))
        self._email.from_email = self._from_addr
        self._email.to = [self._recipient]
        self._email.subject = self.self._subject

