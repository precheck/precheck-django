/**
 * Created by Hramirez on 6/9/2017.
 */

    var moz_server = "http://172.16.16.61";
    var agent_mover_namespace = "/agent_moving";
    var data = {};

    var currently_moving_table = $("#currently_moving_table").DataTable({
         columns: [
             {"data": "Id"},
             {"data": "User"},
             {"data": "SectionKeyID"},
             {"data": "AgentID"},
             {"data": "Source"},
             {"data": "Target"},
             {"data": "Status"},

        ],
        columnDefs: [
            {
                targets: [0],
                visible: false,
                searchable: false
            }
        ]
    });
    var agent_table = $("#move_agent_table").DataTable({


         columns: [
            {"data": "Id"},
            {"data": "User"},
            {"data": "SectionKeyID"},
            {"data": "AgentID"},
            {"data": "Source"},
            {"data": "Target"},
            {"data": "Status"},
            {"data": "CollectionID"},
            {"data": "CollectionViewID"},
            {"data": "OutputCollectionID"},
             {"data": "ViewID"},
            {"data": "SearchType"},
            {"data": "SearchOrderID"},
            {"data": "Section"},
            {"data": "IsAutomationEnabled"}

        ],
        columnDefs: [
            {
                targets: [0,1,6,7,8,9,10,11,12,13,14],
                visible: false,
                searchable: false
            }
        ],
        select: {
            style: 'multi'
        },
        dom: "Bfrtip",
        buttons: [{
               "text": "Delete",
               "className": "btn btn-warning",
               action: function ( e, dt, node, config ) {
                   var rows = this.rows(".selected").data().toArray();
                    for(index = 0; index < rows.length; index++) {
                        var current_item = rows[index];
                        delete data[current_item["AgentID"] + current_item["Source"].toLowerCase()]
                    }
                   this.rows(".selected").remove().draw()
               }
           }, {"text": "Clear Table",
               "className": "btn btn-danger",
               action: function (e, dt, node, config) {
                   this.clear().draw();
                     data = {}

               }}]
    });

    var format_agent_ids = function (data) {
         if (data.loading) return data.text;
        var markup = '<div class="row">' +
            '<div class="col-sm-12">' +
                '<div>' + data.id + '  ' + ' ~ ' + data.name   +'</div>' +
            '</div>' +
                '</div>'

    return markup;
    }



var agent_id = function() {$("#agentid_input").select2({
    dropdownAutoWidth : true,
    width: 'auto',
    placeholder: "Agent",
    allowClear: true,
    ajax: {
        delay: 250,
        url:  function () {
            var environment = $("#source_env").val().toLowerCase()
            return "http://172.16.16.61/api/mozenda/db_utils/" + environment + "/get_info"
        },
        data: function (params) {
            return {
                column_key: "Name",
                identifiers: params.term,
                table: "Agents",
                exact_match: "no",
                columns: "ItemID,Name"

            }
        },
        processResults: function (data) {
            var parsed_data = []
            var items = data.data
            for (var index = 0; index < items.length; index++) {
                var current_item = items[index]
                    if("ItemID" in current_item) {
                        parsed_data.push({
                        "id": current_item.ItemID,
                        "text": current_item.Name ,
                        "name": current_item.Name

                    })
                    }

            }
            return {results: parsed_data};

        }
    },
    escapeMarkup: function (markup) { return markup; },
    templateResult: format_agent_ids
})
    }
agent_id()
$("#source_env").on("change", function () {
    $("#agentid_input").val('').trigger("change");
    $("#agentid_input").select2("destroy")
    agent_id()
    // $("#agentid_input").empty().trigger("change");
    // $('#agentid_input').html('').select2({data: {id:null, text: null}});
    // $("#agentid_input").select2("data", null).trigger("change");
    $("#agentid_input").select2("val", null).trigger("change");
    // $("#agentid_input option").remove();


})


    function get_user_moved_agents(current_user) {
          $.ajax({
              "method": "GET",
              "url": moz_server + "/api/mozenda/cache/moving/user_currently_moving",
              "data": {"user_id": current_user},
              success:function (data) {

                  if(data.data.length > 0) {
                      var items = [];
                      for(var index=0; index < data.data.length; index++) {
                          var info  = data.data[index].split("_");
                          items.push({
                              "Id": info[0] + ":" + info[1] + ":" + info[3],
                              "Source": info[0],
                              "Target": info[1],
                              "SectionKeyID": info[2],
                              "AgentID": info[3],
                              "Status": "moving",
                              "User": info[4]
                          })
                      }

                      currently_moving_table.rows.add(items).draw()

                  }

              }
          })
    }

    function add_to_table(current_user) {

        var agent_id = $("#agentid_input").select2("data")[0].id;
        var source_env = $("#source_env").val();
        var target_env = $("#target_env").val();

        console.log(source_env)

        if(source_env === target_env) {
            alert("source environment cannot be the same as the target environment");
            return
        }
         $.ajax({
             "method": "GET",
             "url": moz_server + "/api/aims/mappings/" + "local_" + source_env.toLowerCase() ,
             "data": {"agent_ids": agent_id},
             success: function (resp_2) {
                 if (resp_2.data.length !== 0) {
                     res_2 = resp_2.data[0];
                     res_2["SectionKeyID"]=   res_2["LicenseState"] + "-" + res_2["LicenseType"]
                     if(res_2["Section"] === $("#section").val()) {
                         $.ajax({
                         "method": "GET",
                         "url": moz_server + "/api/mozenda/cache/staging/is_staged",
                         "data": {
                             "agent_id": agent_id,
                             "source_env": source_env,
                             "target_env": target_env,
                             "section_key": res_2["SectionKeyID"]
                         },
                         success: function (staging_response) {
                             if (staging_response.data === "Not staged") {

                                 delete res_2.status;
                                 delete res_2.id;
                                 delete res_2.date_mapped;

                                 console.log(staging_response)

                                 res_2["Source"] = source_env;
                                 res_2["Target"] = target_env;
                                 res_2["Status"] = "moving";
                                 res_2["Id"] = source_env + ":" + target_env + ":" + agent_id;
                                 res_2["User"] = current_user;

                                 $.ajax({
                                     "type": "GET",
                                     "url": moz_server + "/api/mozenda/cache/moving/is_moving",
                                     "data": {
                                         "agent_id": agent_id,
                                         "source_env": source_env,
                                         "target_env": target_env,
                                         "section_key": res_2["SectionKeyID"],
                                         "user_id": current_user
                                     },
                                     "success": function (moving_response) {
                                         if(moving_response.data === "Not moving")
                                            agent_table.row.add(res_2).draw()
                                     }
                                 })

                             } else {
                                 alert("Agent ID is not mapped for the current environment")
                             }
                         }
                     })
                     }


                 } else {
                     alert("Agent Is Not Mapped please map")
                 }
             }
         })
    }

     $("#move_agent_table tbody").on("click", "tr", function () {
         $(this).toggleClass("selected")

     });

     var agent_mover = io.connect(moz_server + "/agent_moving", {path: '/moz_sockets'});

     // var mover = agent_mover.of("/agent_moving")

     function move_agents(current_user) {
         var to_be_moved = agent_table.rows().data().toArray();
         //  $.ajax({
         //         "method": "POST",
         //         "data": JSON.stringify({"agents": to_be_moved, "user_id": current_user}),
         //         "url": moz_server + "/api/mozenda/cache/moving/add_global_moving_agents"
         //  })
         //
         // $.ajax({
         //     "method": "POST",
         //     "data": JSON.stringify({"agents": to_be_moved, "user_id": current_user}),
         //     "url": moz_server + "/api/mozenda/cache/moving/add_user_moving_agents"
         // })

          agent_table.clear().draw();
          agent_mover.emit('all_moved', {
             "agents": to_be_moved,
             "user_id": current_user
         })
     }

     agent_mover.on('move_to_moving_table', function (data) {
         currently_moving_table.rows.add(data.agents).draw()
         $.ajax({
             "type": "POST",
             "url": moz_server + "/api/tasks/mozenda/agents/move_agents",
             "data": JSON.stringify({"agents": data.agents, "user_id": data.user_id})
         })

     });

     agent_mover.on("agent_status", function (data) {

         var agent_index = currently_moving_table.row(function (idx, row_data, node) {
             return row_data.Id === data.agent_id
         }).index();

         var agent_data = currently_moving_table.row(agent_index).data();
         console.log(data)
         agent_data.Status = data.status;
         currently_moving_table.row(agent_index).invalidate().data(agent_data).draw()

     });

