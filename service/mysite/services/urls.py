from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'home/$', views.index, name='service_home'),
    url(r'^harrislogs/', views.harrislogs, name='harrislogs'),
    url(r'^get_mappings/', views.mappings, name='mappings')
]