# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    username = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    action_time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.


class CcSearchTypes(models.Model):
    search_type = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_search_types'


class ProdMappings(models.Model):
    date_mapped = models.DateField(blank=True, null=True)
    licensestate = models.CharField(db_column='LicenseState', max_length=60, blank=True, null=True)  # Field name made lowercase.
    licensetype = models.CharField(db_column='LicenseType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    agentid = models.CharField(db_column='AgentID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    collectionid = models.CharField(db_column='CollectionID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    collectionviewid = models.CharField(db_column='CollectionViewID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    outputcollectionid = models.CharField(db_column='OutputCollectionID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    viewid = models.CharField(db_column='ViewID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    searchtype = models.CharField(db_column='SearchType', max_length=80, blank=True, null=True)  # Field name made lowercase.
    searchsequence = models.CharField(db_column='SearchSequence', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prod_mappings'


class TestMappings(models.Model):
    date_mapped = models.DateField(blank=True, null=True)
    licensestate = models.CharField(db_column='LicenseState', max_length=50, blank=True, null=True)  # Field name made lowercase.
    licensetype = models.CharField(db_column='LicenseType', max_length=70, blank=True, null=True)  # Field name made lowercase.
    agentid = models.CharField(db_column='AgentID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    collectionid = models.CharField(db_column='CollectionID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    collectionviewid = models.CharField(db_column='CollectionViewID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    outputcollectionid = models.CharField(db_column='OutputCollectionID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    viewid = models.CharField(db_column='ViewID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    searchtype = models.CharField(db_column='SearchType', max_length=60, blank=True, null=True)  # Field name made lowercase.
    searchsequence = models.CharField(db_column='SearchSequence', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'test_mappings'


class Users(models.Model):
    timestamp = models.DateTimeField(blank=True, null=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(unique=True, max_length=100, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
