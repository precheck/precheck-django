from django.shortcuts import render
from .scripts.logreader import HarrisWatcher
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url='login')
def index(request):
    return render(request, "services/index.html", {})

@login_required(login_url='login')
def mappings(request):
    return render(request, "services/mappings.html", {})

@login_required(login_url='login')
def harrislogs(request):
    harris_watcher = HarrisWatcher()
    return JsonResponse({'data': harris_watcher.get_logs()})
