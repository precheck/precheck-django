/**
 * Created by Hramirez on 7/10/2017.
 */

var SERVER_URL = "172.16.16.61"


var update_table = $("#update_table").DataTable({
    columns: [
        {"data": "Id"},
        {"data": "User"},
        {"data": "Environment"},
        {"data": "AgentID"},
        {"data": "Name"},
        {"data": "Section"},
        {"data": "SearchType"},
        {"data": "LicenseType"},
        {"data": "SearchOrderID"},
        {"data": "Status"}
            ],
       columnDefs: [
        {
            targets:[0],
            visible: false,
            searchable: false
        }

    ],
})

var staging_table = $("#staging_table").DataTable({
    columns: [
        {"data": "Id"},
        {"data": "User"},
        {"data": "Environment"},
        {"data": "AgentID"},
        {"data": "Name"},
        {"data": "Section"},
        {"data": "SearchType"},
        {"data": "LicenseType"},
        {"data": "SearchOrderID"},
        {"data": "Status"}
    ],
    columnDefs: [
        {
            targets:[0,1,9],
            visible: false,
            searchable: false
        }

    ],

     dom: "Bfrtip",
        buttons: [{
               "text": "Delete",
               "className": "btn btn-warning delete",
               action: function ( e, dt, node, config ) {
                   this.rows(".selected").remove().draw()
               }
            }],
    select: {
            style: 'multi'
        }
})

//A.V
var env =  $("#env_category").select2({
    data: [
        {id: "test", text:"Test"},
        {id: "prod", text:"Prod"}
    ],
    width: '20%'
})


var section = $("#lic_category").select2({
    data: [
        {id: "License", text: "CC"},
        {id: "CRIM", text: "CRIM"}
    ],
    width: '20%'
})

var format_lic_types = function (data) {
         if (data.loading) return data.text;
        var markup = '<div class="row">' +
            '<div class="col-sm-12">' +
                '<div>' + data.id + '  ' + ' -- ' + data.description   +'</div>' +
            '</div>' +
                '</div>'

    return markup;
    }

var lic_type  = $("#lic_type").select2({
    dropdownAutoWidth : true,
    width: 'auto',
    minimumInputLength: 1,
    placeholder: "License Type",
    ajax: {
        url: "http://172.16.16.61/api/aims/db_utils/prod/get_licenses",
        contentType: 'json',
        delay: 250,
        data: function (params) {
            return {
                license_type: params.term,
            }

        },
        processResults: function (data, params) {
            var parsed_data = []
            for (var index = 0; index < data.data.length; index++) {
                parsed_data.push({
                    "id": data.data[index].license_type,
                    "text": data.data[index].license_type,
                    "description": data.data[index].description

                })
            }
            return {results: parsed_data};
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    templateResult: format_lic_types
})

var search_type = $("#search_type").select2({
        dropdownAutoWidth: true,
        width: 'auto',
        placeholder: "Search Type",
        ajax: {
            url: "http://172.16.16.61/api/aims/db_utils/search_types",
            contentType: "json",
            delay: 250,
            data: function (params) {
                return {search_type: params.term}

            },
            processResults: function (data, params) {
                var parsed_data = []
                for (var index = 0; index < data.data.length; index++) {
                    parsed_data.push({
                        "id": data.data[index].search_type,
                        "text": data.data[index].search_type,

                    })
                }
                return {results: parsed_data};
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
})




var format_agent_ids = function (data) {
         if (data.loading) return data.text;
        var markup = '<div class="row">' +
            '<div class="col-sm-12">' +
                '<div>' + data.id + '  ' + ' ~ ' + data.name   +'</div>' +
            '</div>' +
                '</div>'

    return markup;
    }

var agent_id = function() {
    $("#agent_id").select2({
        dropdownAutoWidth: true,
        width: 'auto',
        placeholder: "Agent",
        ajax: {
            url: function () {
                var environment = env.select2("data")[0].text.toLowerCase()
                return "http://172.16.16.61/api/mozenda/db_utils/" + environment + "/get_info"
            },
            data: function (params) {
                return {
                    column_key: "Name",
                    identifiers: params.term,
                    table: "Agents",
                    exact_match: "no",
                    columns: "ItemID,Name"

                }
            },
            processResults: function (data) {
                var parsed_data = []
                var items = data.data
                for (var index = 0; index < items.length; index++) {
                    var current_item = items[index]
                    if ("ItemID" in current_item) {
                        parsed_data.push({
                            "id": current_item.ItemID,
                            "text": current_item.Name,
                            "name": current_item.Name

                        })
                    }

                }
                return {results: parsed_data};

            }
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: format_agent_ids
    })
}
agent_id()

env.on("change", function () {
    agent_id()

})

function add_mapping(user) {
    var agent_id = $("#agent_id").select2("data")[0].id
    var environment = env.select2("data")[0].text

    var new_data = {
        "Id": agent_id + ":" + $("#search_order").val() +":"+ environment.toLowerCase(),
        "User": user,
        "Name": $("#agent_id").select2("data")[0].name,
        "Environment": environment,
        "Section": section.select2("data")[0].id,
        "AgentID": agent_id,
        "SearchType": search_type.select2("data")[0].text,
        "LicenseType": lic_type.select2("data")[0].text,
        "SearchOrderID": $("#search_order").val(),
        "Status": "Preparing"
    }
    staging_table.row.add(new_data).draw()

}


var moz_server = "http://172.16.16.61"
var agent_mapper = io.connect(moz_server + "/agent_mapping", {path: '/aims_sockets'});



agent_mapper.on("move_to_table2", function (data) {
     update_table.rows.add(data["members"]).draw()
})

agent_mapper.on("mapping_status", function (data) {
    var agent_index = update_table.row(function (idx, row_data, node) {
             return row_data.Id === data.agent_key
         }).index();
    var agent_data = update_table.row(agent_index).data();
         agent_data.Status = data.message;
         update_table.row(agent_index).invalidate().data(agent_data).draw()


})

$("#map").on("click", function () {
         var items = staging_table.rows().data().toArray()
         staging_table.clear().draw()
         agent_mapper.emit('start_mapping', {"data": items})
        setTimeout(function() {
            var url = "http://172.16.16.61/api/tasks/aims/tools/map_agents"
        $.ajax({
            url: url,
            data: JSON.stringify({"agents": items}),
            method: "POST",
            success: function (data) {
            }
        })

            }, (3 * 1000));

})


