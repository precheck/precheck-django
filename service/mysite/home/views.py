from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required(login_url='login')
def index(request):
    return render(request, 'index.html', {})

@login_required(login_url='login')
def mappings(request):
    return render(request, 'services/mappings.html', {})

@login_required(login_url='login')
def services(request):
    return render(request, 'services.html', {})

def login(request):
    return render(request, 'login.html', {})

@login_required(login_url='login')
def logout(request):
    return render(request,'logout.html', {})
