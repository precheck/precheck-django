import datetime
import socket
import abc
import requests
import re
from bs4 import BeautifulSoup

from lxml import etree

class ResponseProcessor(metaclass=abc.ABCMeta):
    def __init__(self, endpoint, collection_helper):
        self._endpoint = endpoint
        self._error_reporter = None
        self._collection_helper = collection_helper


    def set_error_reporter(self, error_reporter):
        self._error_reporter = error_reporter

    def get_errors(self):
        self._error_reporter.get_errors()

    def _get_response(self, agent_id):
        response = requests.get(self._endpoint, params={"Operation": "Agent.Get",
                                                       "AgentID": agent_id})
        return BeautifulSoup(response.text, "lxml")

    def _response_is_success(self, response):
        response_code = response.find("result").text
        return response_code.lower() == "success"

    def _get_extra_info(self, response):
        agent_id = response.find("agentid").text
        input_view_id = response.find("listname", text=re.compile(r"\(\d+\)")).text
        input_id = re.search(r"\((\d+)\)", input_view_id).group(1)
        return agent_id, input_id

    def _process_response(self, response, update_criteria):
        try:
            name = response.find("name").text
            url = response.find("domain").text
            state, target = self._process_name_(name)
            if update_criteria.lower() in ("all", "mappings"):
                agent_id, input_view_id = self._get_extra_info(response)
                data = self._collection_helper.get_data(agent_id, input_view_id)
                return {
                    "name": name,
                    "state": state,
                    "target": target,
                    "url": url,
                    **data
                }
        except (AttributeError, IndexError,ValueError):
            return

    def _process_name_(self, name):
        agent_name = name.split(" ", 1)[0]
        section_id = agent_name.split("-", 1)[1]
        print(section_id)
        state, target = section_id.split("-", 1)
        return state, target

    def processed_rows(self, data, update_criteria=None):
        date_now = str(datetime.datetime.now().date())
        try:
            for row in data:
                response = self._get_response(row["AgentID"])
                if self._response_is_success(response):
                    processed_data = self._process_response(response, update_criteria)
                    if processed_data:
                        row["created"] = date_now
                        row.update(processed_data)
                        yield row
                    else:
                        row["error"] = "Badly formatted Name or Collection Name Problem"
                        self._error_reporter.add_errored_item(row)
                else:
                    logging.debug(row)
                    row["Error"] = "This agent was not found. please check the Agent ID"
                    self._error_reporter.add_errored_item(row)

        except socket.gaierror:
            self._error_reporter.add_errored_item("Mozenda is Down")

        finally:
            self._error_reporter.send_mail_if_any()


class ProcessorFactory:
    @staticmethod
    def get_processor(env, endpoint):
        if env == "test":
            collection_helper = CollectionHelper(endpoint)
        elif env == "production":
            collection_helper = CollectionHelper(endpoint)
        return ResponseProcessor(endpoint, collection_helper)




class AbstractBaseCollectionHelper(metaclass=abc.ABCMeta):
    def __init__(self, endpoint):
        self._endpoint = endpoint
        self._collection_list = None

    @abc.abstractmethod
    def _extract_info(self, input_data, output_data):
        pass

    def get_data(self, agent_id, view_id):
        input_data = self._find_collection_data(("DefaultViewID", view_id))
        output_data = self._find_collection_data(("AgentID", agent_id))
        return self._extract_info(input_data, output_data)

    def _find_collection_data(self, params):
        xpath = ".//{0}[contains(., '{1}')]".format(params[0], params[1])
        collection_list = self._get_collection_list()
        return collection_list.xpath(xpath)[0].getparent()

    def _get_collection_list(self):
        if self._collection_list is None:
            data = requests.get(self._endpoint, params={
                "Operation": "Collection.GetList"
            })

            self._collection_list = etree.fromstring(data.content).getroottree()
        return self._collection_list



class CollectionHelper(AbstractBaseCollectionHelper):

    def _extract_info(self, input_data, output_data):
        return {
            "CollectionID": input_data.find(".//CollectionID").text,
            "InputCollectionViewID": input_data.find(".//DefaultViewID").text,
            "OutputCollectionID": output_data.find(".//CollectionID").text,
            "ViewID": output_data.find(".//DefaultViewID").text
        }




class BaseDataHandler(metaclass=abc.ABCMeta):
    def __init__(self):
        self._formatted_data = list()


    def get_formated_data(self):
        return self._formatted_data



    @abc.abstractmethod
    def _data_formatter(self):
        pass


    def format_data(self, data):
        formatter = self._data_formatter()
        formatted_data = formatter(data)
        return formatted_data

    def add_data(self, data):
        self._formatted_data.append(data)


class CCMappingHandler(BaseDataHandler):
    tied_model = None

    def _data_formatter(self):
        return lambda row: {
            "date_mapped": row["created"],
            "LicenseState": row["state"],
            "LicenseType": row["LicenseType"],
            "AgentID": row["AgentID"],
            "CollectionID": row["CollectionID"],
            "ViewID": row["ViewID"],
            "SearchType": row["SearchType"],
            "SearchSequence": row["SearchSequence"],
            "OutputCollectionID": row["OutputCollectionID"],
            "CollectionViewID": row["InputCollectionViewID"]
        }


class ProdMappingDataHandler(CCMappingHandler):
    tied_model = "prod_mappings"


class TestMappingDataHandler(CCMappingHandler):
    tied_model = "test_mappings"

class CrimMappingDataHandler(BaseDataHandler):
    tied_model = "crim_mappings"

    def _data_formatter(self):
        return lambda row: {
            "date_mapped": row["created"],
            "state": row["state"],
            "county_id": row["CountyID"],
            "county": row["target"],
            "AgentID": row["AgentID"],
            "CollectionID": row["collection_id"],
            "ViewID": row["ViewID"],
        }

class BaseAllAgentsDataHandler(BaseDataHandler):
    def _data_formatter(self):
        return lambda row: {
            "agent_id": row["AgentID"],
            "origin": "",
            "section": row["Section"],
            "name": row["name"],
            "created": row["created"],
            "author": row["Author"],
            "url": row["url"],
            "state": row["state"],
            "status": row["AgentStatus"],
            "tag": row["Tag"]
        }


class ProdAllAgentsDataHandler(BaseAllAgentsDataHandler):
    tied_model = "all_agents"

class TestAllAgentsDataHandler(BaseAllAgentsDataHandler):
    tied_model = "test_all_agents"



class HandlerFactory:
    @staticmethod
    def get_handlers(update_criteria,  section, env):
        if update_criteria == "All":
            if section == "CC":
                return ProdAllAgentsDataHandler(), TestMappingDataHandler() if env == "test" else ProdMappingDataHandler()
            else:
                return ProdAllAgentsDataHandler(), CrimMappingDataHandler()

        elif update_criteria == "Mappings":
            if section == "CC":
                return TestMappingDataHandler() if env == "test" else ProdMappingDataHandler(),
            else:
                return CrimMappingDataHandler(),
        else:
            return ProdAllAgentsDataHandler(),





