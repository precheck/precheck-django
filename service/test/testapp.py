import sqlalchemy
from sqlalchemy.orm import session, sessionmaker
from sqlalchemy import MetaData, Table, create_engine
import json
from pprint import pprint

with open('config.json') as config_file:
    config = json.load(config_file)
pprint(config)

engine = create_engine(f"mssql+pyodbc://{config['test_db']['user']}:{config['test_db']['password']}@hou-sqltest-01/precheck?driver=SQL+Server+Native+Client+11.0")
session = sessionmaker(bind=engine)()

mappings = Table("DataXtract_RequestMapping", MetaData(), autoload_with=engine)

if __name__ == "__main__":
    data = session.query(mappings).first()
    print(data)


