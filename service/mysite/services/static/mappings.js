$(document).ready(function(){
	var SERVER_URL = "172.16.16.61"
	var table = $('#mappings').DataTable( {
		columns: [
			{"data": "Section"},
			{"data": "SectionKeyID"},
			{"data": "SearchOrderID"},
			{"data": "AgentID"},
			{"data": "SearchType"},
			{"data": "CollectionID"},
			{"data": "CollectionViewID"},
			{"data": "OutputCollectionID"},
			{"data": "ViewID"},
			{"data": "IsAutomationEnabled"}
			],
		dom: "Bfrtip",

		"ajax":{
			url: "http://" + SERVER_URL+ "/api/aims/mappings/" + $('#mapdropdown').val()
        },
		buttons: [{
               "text": "Refresh",
               "className": "btn btn-warning",
               action: function () {
    				$.ajax({
						method:"POST",
						url: "http://" + SERVER_URL+ "/api/aims/mappings/" + $('#mapdropdown').val() + "/clear_mapping_cache",
						success: function () {
							new_url = "http://" + SERVER_URL+ "/api/aims/mappings/" + $('#mapdropdown').val()
							table.ajax.url(new_url).load();

                        }
					})

               }}],
		"processing": true,
		"language": { 
			"loadingRecords": "&nbsp;",
			"processing": "Loading..."},
		});
		$('#mapdropdown').change(function() {
			new_url = "http://" + SERVER_URL+ "/api/aims/mappings/" + $('#mapdropdown').val()
			table.ajax.url(new_url).load();
		});
});




